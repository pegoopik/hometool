package com.pegoopik.hometool.audiobook.executors;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class SyncCopyFiles {

    private static final String FROM_PATH = "/home/pegoopik/Downloads/Dune_God_1/";
    private static final String TO_PATH = "/media/pegoopik/9016-4EF8/mp3/Dune_God/";

    public static void main(String[] args) throws IOException {
        File fromPath = new File(FROM_PATH);
        File toPath = new File(TO_PATH);
        if (!toPath.exists()) {
            toPath.mkdirs();
        }
        if ((!fromPath.exists()) || fromPath.isFile()) {
            throw new RuntimeException("(!fromPath.exists()) || fromPath.isFile()");
        }
        if (toPath.isFile()) {
            throw new RuntimeException("toPath.isFile()");
        }
        Map<String, File> sortedFiles = new TreeMap<>();
        for (final File file : fromPath.listFiles()) {
            sortedFiles.put(file.getName(), file);
        }
        for (final Map.Entry<String, File> fileEntry : sortedFiles.entrySet()) {
            String prepFileName = prepareFileName(fileEntry.getKey());
            FileUtils.copyFile(fileEntry.getValue(),
                    new File(TO_PATH + prepFileName));
            System.out.println(prepFileName + " copied");
        }
        System.out.println("Done!!!");
    }

    public static String prepareFileName(String fileName) {
        //TODO: сделать нормальный метод и перенести в модуль str
        return fileName
                .replace(" ", "_")
                .replace("<", "_")
                .replace(">", "_")
                .replace("__", "_")
                .replace("__", "_");
    }

}
