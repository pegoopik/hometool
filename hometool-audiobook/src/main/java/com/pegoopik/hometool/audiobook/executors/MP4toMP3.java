package com.pegoopik.hometool.audiobook.executors;

import it.sauronsoftware.jave.*;

import java.io.File;


public class MP4toMP3 {


    public static void main(String[] args) throws EncoderException {
        int currStartIndex = 1;
        currStartIndex = convertMP4toAudioBook(new File("/home/pegoopik/Downloads/Dune_God_1.mp4"),
                "/home/pegoopik/Downloads/Dune_God", 600F, currStartIndex);
        currStartIndex = convertMP4toAudioBook(new File("/home/pegoopik/Downloads/Dune_God_2.mp4"),
                "/home/pegoopik/Downloads/Dune_God", 600F, currStartIndex);
        convertMP4toAudioBook(new File("/home/pegoopik/Downloads/Dune_God_3.mp4"),
                "/home/pegoopik/Downloads/Dune_God", 600F, currStartIndex);
    }

    public static void main2(String[] args) {
        // /home/pegoopik/Downloads/Dune_God/
//        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God_2.mp4"),
//                new File("/home/pegoopik/Downloads/Dune_God/038_1.mp3"), 4, 598L);
//        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God_2.mp4"),
//                new File("/home/pegoopik/Downloads/Dune_God/038_2.mp3"), 5, 597L);
//        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God_2.mp4"),
//                new File("/home/pegoopik/Downloads/Dune_God/038_3.mp3"), 6, 596L);
//        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God_3.mp4"),
//                new File("/home/pegoopik/Downloads/Dune_God/077_1.mp3"), 1, 605L);
//        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God_3.mp4"),
//                new File("/home/pegoopik/Downloads/Dune_God/077_2.mp3"), 2, 604L);
//        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God_3.mp4"),
//                new File("/home/pegoopik/Downloads/Dune_God/077_3.mp3"), 3, 603L);
        convertMp4ToMp3(new File("/home/pegoopik/Downloads/Dune_God/077.mp3"),
                new File("/home/pegoopik/Downloads/Dune_God/077_1.mp3"), 1, null);
    }

    public static int convertMP4toAudioBook(File source, String targetFolder, float fileDurationS, int startIndex) throws EncoderException {
        Encoder forMusic = new Encoder();
        MultimediaInfo info = forMusic.getInfo(source);
        float sumDurationS = info.getDuration() / 1000F;
        long fileCount = Math.round(sumDurationS / fileDurationS);
        long preparedFileDuration = Math.round(sumDurationS / fileCount);
        System.out.println("source.getName(): " + source.getName());
        System.out.println("fileDurationS: " + fileDurationS);
        System.out.println("sumDuration: " + sumDurationS);
        System.out.println("fileCount: " + fileCount);
        System.out.println("preparedFileDuration: " + preparedFileDuration);
        File targetFolderFile = new File(targetFolder);
        if (!targetFolderFile.exists()) {
            targetFolderFile.mkdirs();
        }
        if (targetFolderFile.isFile()) {
            throw new RuntimeException("targetFolderFile.isFile()");
        }
        for (int i = 0; i < fileCount; i++) {
            long offset = i * preparedFileDuration;
            String currFileName = (targetFolder + "/" + getIndexStr(startIndex + i, 3) + ".mp3")
                    .replace("//", "/");
            System.out.println("Create file: " + offset + " " + (preparedFileDuration - 1L)
                    + "\t" + currFileName);
            if (i + 1 == fileCount) {
                convertMp4ToMp3(source, new File(currFileName), offset, null);
                return startIndex + i + 1;
            } else {
                convertMp4ToMp3(source, new File(currFileName), offset, preparedFileDuration - 1L);
            }
        }
        return -1;
    }

     public static String getIndexStr(int index, int length) {
         return ("" + Math.round(Math.pow(10, length) + index)).substring(1);
     }

    public static void convertMp4ToMp3(File source, File output, long offsetS, Long durationS) {
        Encoder forMusic = new Encoder();


        EncodingAttributes specifications = new EncodingAttributes();
        specifications.setFormat("mp3");
        specifications.setOffset((float) offsetS);
        if (durationS != null) {
            specifications.setDuration((float) durationS);
        }
        //audioAttribute obj
        AudioAttributes a = new AudioAttributes();
        a.setBitRate(64);
        a.setCodec("mp2");

        specifications.setAudioAttributes(a);

        try{
            forMusic.encode( source, output, specifications);
        }
        catch(EncoderException ex){
            ex.printStackTrace();
        }
    }

}
